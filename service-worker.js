importScripts('/assets/js/workbox-sw.js')

// workbox.setConfig({
//   modulePathPrefix: '/assets/js/'
// })

workbox.precaching.precacheAndRoute([
  {url: '/index.html', revision: 110 },
  {url: '/assets/css/style.css?v=1.3', revision: 131 },
  {url: '/assets/js/localforage.min.js', revision: null},
  {url: '/assets/js/signature_pad.min.js', revision: null},
  {url: '/assets/js/main.js?v=1.1', revision: 110},
  {url: '/assets/fonts/marianne/Marianne-Regular.woff2', revision: null},
  {url: '/assets/fonts/marianne/Marianne-Bold.woff2', revision: null}
]);

